import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import Swiper from 'swiper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  title = 'new-project';

  alley = [{ title: 'A' }, { title: 'C' }];

  public Post: any = [];
  config: SwiperConfigInterface = {
    effect: 'coverflow',
    direction: 'horizontal',
    slidesPerView: 3,
    slideToClickedSlide: true,
    mousewheel: true,
    scrollbar: false,
    watchSlidesProgress: true,
    navigation: true,
    keyboard: true,
    pagination: false,
    centeredSlides: true,
    loop: true,
    roundLengths: true,
    slidesOffsetBefore: 100,
    slidesOffsetAfter: 100,
    spaceBetween: 50,
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
      },
    },
  };

  constructor(private http: HttpClient) {}

  ngOnInit() {
    const subscribe = this.http.get(
      'https://jsonplaceholder.typicode.com/posts'
    );
    subscribe.subscribe((data: any) => {
      this.Post = data;
    });

    const mySwiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      loop: true,
      lazy: true,
      centeredSlides: true,
      grabCursor: true,
      slidesPerView: 2,
      coverflowEffect: {
        rotate: 1,
        stretch: 0,
        // depth: 1000,
        depth: 500,
        modifier: 1,
        slideShadows: true,
      },
      mousewheel: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      observer: true,
    });
  }
}
